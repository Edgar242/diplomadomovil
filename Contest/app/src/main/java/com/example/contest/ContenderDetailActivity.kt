package com.example.contest

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_contender_detail.*

class ContenderDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contender_detail)

        textViewDataName.text = "Nombre: ${intent.getStringExtra("name")}"
        textViewDataAge.text = "Edad: ${intent.getStringExtra("age")}"
        textViewDataCountry.text = "País: ${intent.getStringExtra("country")}"
        textViewDataPoints.text = "Puntos: ${intent.getStringExtra("points")}"
        imageViewDataProfile.setImageResource(intent.getIntExtra("imageSrcId", R.drawable.ic_launcher_background))

        buttonBack.setOnClickListener() {
            intent = Intent(this, MainActivity::class.java )
            startActivity(intent)
        }
    }
}