package com.example.contest

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.contest.adapters.ContenderAdapter
import com.example.contest.listeners.ContenderListener
import com.example.contest.models.Contender
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), ContenderListener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val contenders = arrayListOf < Contender > (
            Contender(1,
                "Garry Kasparov",
                23,
                "Rusia",
                2740,
                R.drawable.face_kasparov
            ),
            Contender(2,
                "Carlos",
                18,
                "USA",
                2150,
                R.drawable.face_putin
            ),
            Contender(3,
                "María",
                25,
                "Francia",
                1739,
                R.drawable.face_girl1
            ),
            Contender(4,
                "Fredy",
                30,
                "Italia",
                2064,
                R.drawable.face_obama
            ),
            Contender(5,
                "Andrea",
                27,
                "Colombia",
                1940,
                R.drawable.face_alexandra
            ),
            Contender(6,
                "Joana",
                24,
                "Argentina",
                2340,
                R.drawable.face_queen
            ),
            Contender(7,
                "Won Yang",
                30,
                "Brasil",
                2274,
                R.drawable.face_chinese
            ),
            Contender(7,
                "Carlsen Magnus",
                28,
                "USA",
                2800,
                R.drawable.face_carlsen_magnus
            ),
        )
        val contenderAdapter = ContenderAdapter(contenders)
        contenderAdapter.setContenderListener(this)
        recyclerViewContenders.adapter = contenderAdapter
        val layoutManager = StaggeredGridLayoutManager(2,RecyclerView.VERTICAL)
        recyclerViewContenders.layoutManager = layoutManager
    }

        override fun onClickContender(contender: Contender) {
            Toast.makeText(this,"Le dio click a la vista", Toast.LENGTH_LONG).show()
        }

        override fun onShowDetails(contender: Contender) {
            Toast.makeText(this,"Name: ${contender.name} ",Toast.LENGTH_LONG).show()
            var intent = Intent(this, ContenderDetailActivity::class.java ).apply {
                val detailBundle = Bundle().apply {
                    putString("name", contender.name)
                    putString("age", contender.age.toString())
                    putString("country", contender.country)
                    putString("points", contender.points.toString())
                    putInt("imageSrcId", contender.imgSrcId)
                }
                putExtras(detailBundle)
            }
            startActivity(intent)
        }
}