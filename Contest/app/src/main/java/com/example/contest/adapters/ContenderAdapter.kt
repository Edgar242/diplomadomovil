package com.example.contest.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.contest.R
import com.example.contest.adapters.viewHolders.ContenderViewHolder
import com.example.contest.listeners.ContenderListener
import com.example.contest.models.Contender


class ContenderAdapter (val contenders: List<Contender>) : RecyclerView.Adapter<ContenderViewHolder>() {

    private var contenderListener: ContenderListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContenderViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_contender, parent, false)
        val contenderViewHolder = ContenderViewHolder(itemView)
        contenderViewHolder.setContenderListener(contenderListener)

        return contenderViewHolder
    }

    override fun onBindViewHolder(holder: ContenderViewHolder, position: Int) {
        holder.bindContender(contenders[position])
    }

    override fun getItemCount(): Int {
        return contenders.size
    }

    fun setContenderListener(listener: ContenderListener?) {
        this.contenderListener = listener
    }
}