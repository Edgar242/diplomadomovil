package com.example.contest.adapters.viewHolders

import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.contest.R
import com.example.contest.listeners.ContenderListener
import com.example.contest.models.Contender

class ContenderViewHolder(val itemView: View) : RecyclerView.ViewHolder(itemView)  {

    private var contenderListener: ContenderListener? = null
    private var contender:Contender? = null

    private var textViewName: TextView = itemView.findViewById(R.id.textViewName)
    private var textViewAge: TextView = itemView.findViewById(R.id.textViewAge)
    private var textViewCountry = itemView.findViewById<TextView>(R.id.textViewCountry)
    private var textViewPoints = itemView.findViewById<TextView>(R.id.textViewPoints)
    private var buttonDetails: Button = itemView.findViewById(R.id.buttonDetails)
    private var imageViewProfile = itemView.findViewById<ImageView>(R.id.imageViewProfile)

    fun bindContender(contender: Contender) {
        this.contender=contender
        textViewName.text = this.contender?.name
        textViewAge.text = this.contender?.age.toString()
        textViewCountry.text = this.contender?.country
        textViewPoints.text = this.contender?.points.toString()
        this.contender?.imgSrcId?.let { imageViewProfile.setImageResource(it) }

        buttonDetails.setOnClickListener{ _ ->
            contenderListener?.onShowDetails(this.contender!!)
        }
    }

    fun setContenderListener(listener: ContenderListener?) {
        this.contenderListener=listener
    }
}