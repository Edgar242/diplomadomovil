package com.example.contest.listeners

import com.example.contest.models.Contender

interface ContenderListener {

    fun onClickContender(contender: Contender)

    fun onShowDetails(contender: Contender)
}