package com.example.contest.models

data class Contender(
    val id: Int = 0,
    val name: String = "noname",
    val age: Int = 0,
    val country: String = "none",
    val points: Int = 0,
    val imgSrcId: Int,
)