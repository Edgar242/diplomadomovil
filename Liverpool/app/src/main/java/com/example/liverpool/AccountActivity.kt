package com.example.liverpool

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.liverpool.databinding.ActivityAccountBinding
import com.example.liverpool.models.User
import com.example.liverpool.models.User.Companion.sharedPrefName

class AccountActivity : AppCompatActivity() {
    private lateinit var binding : ActivityAccountBinding
    private lateinit var sharedPreferences : SharedPreferences
    private lateinit var user : User

    companion object {
        const val REQUEST_SIGN = 200
        const val REQUEST_SIGN_UP = 201
        const val REQUEST_SIGN_IN = 202
        const val RESULT_SIGN_UP_OK = 301
        const val RESULT_SIGN_IN_OK = 302
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountBinding.inflate(layoutInflater)
        setContentView(binding.root)

        sharedPreferences = getSharedPreferences(sharedPrefName, MODE_PRIVATE)
        user = User(sharedPreferences)
        binding.textViewUserName.visibility = if (user.isLogged) View.VISIBLE else View.INVISIBLE
        binding.textViewUserLastName.visibility = if (user.isLogged) View.VISIBLE else View.INVISIBLE
        binding.textViewUserEmail.visibility = if (user.isLogged) View.VISIBLE else View.INVISIBLE
        binding.buttonLogOut.visibility = if (user.isLogged) View.VISIBLE else View.INVISIBLE

        if (user.isLogged) {
            showUserInfo()
        } else {
            intent = Intent(this, AccountUserRegisterActivity::class.java)
            startActivityForResult(intent, REQUEST_SIGN)
        }
    }

    private fun showUserInfo() {
        setTitle("¡Hola ${user.name}!")

        binding.textViewUserName.text = user.name
        binding.textViewUserLastName.text = user.lastName
        binding.textViewUserEmail.text = user.email

        binding.buttonLogOut.setOnClickListener {
            user.logOut()
            refresh()
        }
    }


    private fun refresh() {
        overridePendingTransition(0, 0)
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        finish()
        overridePendingTransition(0, 0)
        startActivity(intent)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // Toast.makeText(this, "$requestCode and $resultCode sign in", Toast.LENGTH_SHORT).show()
        if (requestCode == REQUEST_SIGN && resultCode == RESULT_CANCELED) {
            // Force to exit and return to all products home screen
            finish()
        }

        if (requestCode == REQUEST_SIGN && resultCode == RESULT_SIGN_IN_OK) {
            refresh()
        }
    }
}