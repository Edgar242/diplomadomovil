package com.example.liverpool

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.example.liverpool.AccountActivity.Companion.RESULT_SIGN_IN_OK
import com.example.liverpool.AccountActivity.Companion.RESULT_SIGN_UP_OK
import com.example.liverpool.databinding.ActivityAccountUserRegisterBinding
import com.example.liverpool.models.User
import com.example.liverpool.models.User.Companion.sharedPrefName

class AccountUserRegisterActivity : AppCompatActivity() {
    private lateinit var b: ActivityAccountUserRegisterBinding
    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        b = ActivityAccountUserRegisterBinding.inflate(layoutInflater)
        setContentView(b.root)

        sharedPreferences = getSharedPreferences(sharedPrefName, Context.MODE_PRIVATE)

        setListenerSignIn()
        setListenerSignUp()
    }

    private  fun setListenerSignIn() {
        b.editTextSignInEmail.addTextChangedListener{
            b.buttonSignIn.isEnabled = isNotEmpty(b.editTextSignInEmail, b.editTextSignInPassword)
        }

        b.editTextSignInPassword.addTextChangedListener{
            b.buttonSignIn.isEnabled = isNotEmpty(b.editTextSignInEmail, b.editTextSignInPassword)
        }

        b.buttonSignIn.setOnClickListener {
            // Set email and password
            var user = User(sharedPreferences)
            if (!user.isEmailRegistered(b.editTextSignInEmail.text.toString())) {
                Toast.makeText(this, "User not registered", Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }
            val result = user.logIn(b.editTextSignInEmail.text.toString(),b.editTextSignInPassword.text.toString())
            if (result) {
                Toast.makeText(this, "Login success!", Toast.LENGTH_LONG).show()
                setResult(RESULT_SIGN_IN_OK)
            } else {
                Toast.makeText(this, "Login failed!", Toast.LENGTH_LONG).show()
            }
        }
    }

    private  fun setListenerSignUp() {
        b.editTextName.addTextChangedListener{
            b.buttonSignUp.isEnabled = isNotEmpty(
                b.editTextName,
                b.editTextLastName,
                b.editTextEmail,
                b.editTextPassword
            )
        }

        b.editTextLastName.addTextChangedListener{
            b.buttonSignUp.isEnabled = isNotEmpty(
                b.editTextName,
                b.editTextLastName,
                b.editTextEmail,
                b.editTextPassword
            )
        }

        b.editTextEmail.addTextChangedListener{
            b.buttonSignUp.isEnabled = isNotEmpty(
                b.editTextName,
                b.editTextLastName,
                b.editTextEmail,
                b.editTextPassword
            )
        }

        b.editTextPassword.addTextChangedListener{
            b.buttonSignUp.isEnabled = isNotEmpty(
                b.editTextName,
                b.editTextLastName,
                b.editTextEmail,
                b.editTextPassword
            )
        }

        b.buttonSignUp.setOnClickListener {
            val user = User(sharedPreferences)
           user.register(
                b.editTextName.text.toString(),
                b.editTextLastName.text.toString(),
                b.editTextEmail.text.toString(),
                b.editTextPassword.text.toString()
            )
            setResult(RESULT_SIGN_UP_OK)
            finish()
        }
    }

    private fun isNotEmpty(vararg editText: EditText) : Boolean {
        var result = true
        editText.forEach {
            result = result && it.text.isNotEmpty()
        }
        return result
    }
}