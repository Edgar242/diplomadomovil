package com.example.liverpool

import android.content.Intent
import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.liverpool.adapters.ProductAdapter
import com.example.liverpool.database.ProductDB
import com.example.liverpool.databinding.ActivityAllProductsBinding
import com.example.liverpool.listener.ProductListener
import com.example.liverpool.models.Product

class MainProductsActivity : AppCompatActivity(), ProductListener {
    private lateinit var binding: ActivityAllProductsBinding
    private lateinit var productAdapter: ProductAdapter
    private lateinit var productDB: ProductDB

    companion object {
        const val REQUEST_MODIFY_PRODUCT = 200
        const val REQUEST_ADD_PRODUCT = 201
        const val REQUEST_EDIT_PRODUCT = 202
        const val RESULT_ADD_PRODUCT_OK = 301
        const val RESULT_EDIT_PRODUCT_OK = 302
        const val RESULT_DELETE_PRODUCT_OK = 303
        const val RESULT_CLEAR_PRODUCT_OK = 304
        const val RESULT_LOADED_PRODUCT_OK = 305
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAllProductsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbarHome)


        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(false)
            setDisplayShowHomeEnabled(true)
        }

        title = "Todos los productos"

        // Set DB
        productDB = ProductDB(baseContext, "productsBD", TypeConnection.WRITABLE)

        displayProducts()

        binding.swipeRefreshLayout.setOnRefreshListener {
            displayProducts()
            binding.swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun displayProducts()  {
        val products = productDB.getAll()
        if (products.size == 0) binding.textView.visibility = View.VISIBLE
        else binding.textView.visibility = View.GONE

        // Set adapter
        productAdapter = ProductAdapter(products, this)
        binding.recyclerViewProducts.adapter = productAdapter

        // Detect orientation and set the number of columns for the layout
        var columns = 2
        var orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            columns = 3
        }

        // Use the layout
        var layoutManager = StaggeredGridLayoutManager(columns, RecyclerView.VERTICAL )
        binding.recyclerViewProducts.layoutManager = layoutManager
    }

    override fun onClickProduct(product: Product) {
        // TODO: Send product to the Activity via bundle
        val intentProductDetail = Intent(baseContext, ProductDetailActivity::class.java).apply {
            val bundleProduct = Bundle().apply {
                putParcelable("product", product)
            }
            putExtras(bundleProduct)
        }
        startActivity(intentProductDetail)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.menuSearch -> {
                Toast.makeText(baseContext, "Buscando ...", Toast.LENGTH_SHORT).show()
            }
            R.id.menuCategory -> {
                intent = Intent(this, ToolbarActivity::class.java)
                startActivity(intent)
            }
            R.id.menuManagement->{
                intent = Intent(this, ProductManagementActivity::class.java)
                startActivityForResult(intent, REQUEST_MODIFY_PRODUCT)
            }
            R.id.menuAccount-> {
                intent = Intent(this, AccountActivity::class.java)
                startActivity(intent)
            }
            R.id.menuAboutUs->{
                Toast.makeText(baseContext, "Acerca de nosotros", Toast.LENGTH_SHORT).show()
            } else-> {
                return super.onOptionsItemSelected(item)
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_CANCELED) {
            displayProducts()
            return
        }

        if (requestCode == REQUEST_MODIFY_PRODUCT) {
            when( resultCode ) {
                RESULT_ADD_PRODUCT_OK -> {
                    data?.let {
                        val product = it.getParcelableExtra<Product>("product")
                        product?.let {
                            productDB.add(product)
                            displayProducts()
                            Toast.makeText(this, "New product added!", Toast.LENGTH_LONG).show()
                        }
                    }
                }
                RESULT_EDIT_PRODUCT_OK -> {
                    data?.let {
                        val index = it.getIntExtra("productIndex", -1)
                        val product = it.getParcelableExtra<Product>("product")
                        if (product != null && index != -1) {
//                            productAdapter.edit(index,product)
                            Toast.makeText(this, "Product edited!", Toast.LENGTH_SHORT)
                        }
                    }
                }
                RESULT_DELETE_PRODUCT_OK -> {
                    data?.let {
                        val product = it.getParcelableExtra<Product>("product")
                        product?.let {
                            productDB.delete(product)
                            Toast.makeText(this, "Product with ID=${product.id} deleted!", Toast.LENGTH_LONG).show()
                        }
                        displayProducts()
                    }
                }
                RESULT_CLEAR_PRODUCT_OK -> {
                    Toast.makeText(this, "All products cleared!", Toast.LENGTH_LONG).show()
                    displayProducts()
                    binding.textView.visibility = View.VISIBLE
                }
                RESULT_LOADED_PRODUCT_OK -> {
                    Toast.makeText(this, "Product examples loaded!", Toast.LENGTH_LONG).show()
                    displayProducts()
                    binding.textView.visibility = View.GONE
                }
            }
        }
    }
}