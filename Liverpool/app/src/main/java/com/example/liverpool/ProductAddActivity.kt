package com.example.liverpool

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.liverpool.database.ProductDB
import com.example.liverpool.databinding.ActivityAddProductBinding
import com.example.liverpool.models.Product

class ProductAddActivity : AppCompatActivity() {
    private lateinit var binding : ActivityAddProductBinding
    private lateinit var productDB: ProductDB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAddProductBinding.inflate(layoutInflater)
        setContentView(binding.root)

        title = "Añadir nuevo producto"
        productDB = ProductDB(baseContext, "productsBD", TypeConnection.WRITABLE)

        binding.buttonAddProduct.setOnClickListener{
            // New product for testing purposes
            val product = Product(
                0, // Autoincremented by DB
                binding.editTextTitle.text.toString(),
                binding.editTextDescription.text.toString(),
                R.drawable.no_image_available,
                binding.editTextCategory.text.toString(),
                binding.editTextPrice.text.toString().toFloat(),
                0.0F
            )
            intent.apply {
                val bundleNewProduct =  Bundle().apply {
                    putParcelable("product", product)
                }
                putExtras(bundleNewProduct)
            }
            setResult(RESULT_OK, intent)
            finish()
        }

        binding.buttonCancel.setOnClickListener{
            setResult(RESULT_CANCELED)
            finish()
        }
    }
}