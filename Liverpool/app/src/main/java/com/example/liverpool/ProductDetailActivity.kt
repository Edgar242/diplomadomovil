package com.example.liverpool

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.liverpool.databinding.ActivityProductDetailBinding
import com.example.liverpool.models.Product

class ProductDetailActivity : AppCompatActivity() {
    private lateinit var binding :ActivityProductDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setTitle("Detalles")

        // Fill the product detail view with product data
        val product = intent.getParcelableExtra<Product>("product")
        product?.let {
            binding.imageViewDetailItem.setImageResource(it.image)
            binding.textViewDetailName.text = it.title
            binding.ratingBarDetailItem.rating = it.rating
            binding.textViewDetailPrice.text = "$" + it.price
            binding.textViewCategory.text = it.category
            binding.textViewDetailDescription.text = it.description
        }

    }
}