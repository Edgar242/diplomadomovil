package com.example.liverpool

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.liverpool.MainProductsActivity.Companion.REQUEST_ADD_PRODUCT
import com.example.liverpool.MainProductsActivity.Companion.RESULT_ADD_PRODUCT_OK
import com.example.liverpool.MainProductsActivity.Companion.RESULT_CLEAR_PRODUCT_OK
import com.example.liverpool.MainProductsActivity.Companion.RESULT_DELETE_PRODUCT_OK
import com.example.liverpool.MainProductsActivity.Companion.RESULT_LOADED_PRODUCT_OK
import com.example.liverpool.adapters.ProductManagementAdapter
import com.example.liverpool.database.ProductDB
import com.example.liverpool.databinding.ActivityManageProductsBinding
import com.example.liverpool.listener.ProductMgmtListener
import com.example.liverpool.models.Product

class ProductManagementActivity : AppCompatActivity(), ProductMgmtListener {
    private lateinit var binding: ActivityManageProductsBinding
    private lateinit var productDB: ProductDB
    private lateinit var productAdapter: ProductManagementAdapter
    private var productToModify = arrayListOf<Product>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityManageProductsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setTitle("Administración")
        productDB = ProductDB(baseContext, "productsBD", TypeConnection.WRITABLE)


        loadData()
    }

    private fun loadData() {
        // Get all products from BD
        val products = productDB.getAll()

        // Set data to adapter
        productAdapter = ProductManagementAdapter(products, this)
        binding.recyclerVewManageProducts.adapter = productAdapter

//        // Detect orientation and set the number of columns for the layout
        var columns = 1
//        var orientation = resources.configuration.orientation
//        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//            columns = 3
//        }
//
//        // Use the layout
        val layoutManager = StaggeredGridLayoutManager(columns, RecyclerView.VERTICAL )
        binding.recyclerVewManageProducts.layoutManager = layoutManager
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.modify_product, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuProductAdd->{
                intent = Intent(baseContext, ProductAddActivity::class.java)
                startActivityForResult(intent, REQUEST_ADD_PRODUCT)
            }
            R.id.menuProductEdit->{
                // TODO: Complete Edit product
                Toast.makeText(this, "Under construction", Toast.LENGTH_SHORT).show()
            }
            R.id.menuProductDelete->{
                if (productToModify.size == 0) {
                    Toast.makeText(this, "Please select the items to delete", Toast.LENGTH_LONG).show()
                    setResult(RESULT_CANCELED)
                    return true
                }
                productToModify.forEach{
                    productDB.delete(it)
                }
                Toast.makeText(this, "Items deleted: ${productToModify.size}", Toast.LENGTH_LONG).show()
                setResult(RESULT_DELETE_PRODUCT_OK)
                loadData()
            }
            R.id.menuProductLoadExamples -> {
                AddProductExamplesToDB()
                setResult(RESULT_LOADED_PRODUCT_OK)
                loadData()
            }
            R.id.menuProductClear -> {
                productDB.clear()
                setResult(RESULT_CLEAR_PRODUCT_OK)
//                finish()
                loadData()
            }
            else->{
                return super.onOptionsItemSelected(item)
            }
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ADD_PRODUCT && resultCode == RESULT_OK) {
            setResult(RESULT_ADD_PRODUCT_OK, data)
            finish()
        }
    }

    private fun AddProductExamplesToDB() {
        var products = arrayListOf<Product>(
            Product(
                    100,
                    "Lente solar para hombre Ray Ban The Colonel",
                    "The Colonel, un icónico modelo con detalles pulcros en su armazón de doble puente. Elaborado en un atractivo acabado con lentes en colores clásicos que denotan su personalidad, elevando con carácter esta emblemática pieza",
                    R.drawable.lentes,
                    "Lentes",
                    2623.2F,
                    4.8F
            ),
            Product(
                101,
                    "Refrigerador Samsung 27.4 pies cúbicos Inverter RF27T5501B1/EM",
                    "Gran capacidad SpaceMax. Abastece tu refrigerador de forma ordenada gracias a su amplio espacio de almacenamiento de 27.4 pies cúbicos. La tecnología SpaceMax permite que las paredes sean mucho más delgadas al utilizar aislamiento de alta eficiencia. Así, ofrece mayor espacio en el interior, sin aumentar las dimensiones exteriores ni comprometer la eficiencia energética. El compresor Digital Inverter de Samsung ofrece mayor durabilidad y ahorra energía hasta en un 50%. A diferencia de los compresores convencionales, ajusta su velocidad automáticamente en respuesta a la demanda de enfriamiento, lo que reduce el desgaste. También cuenta con la certificación VDE por una durabilidad de 21 años",
                    R.drawable.refrigerador,
                "Refrigeradores",
                    51539.40F,
                    4.3F
            ),
            Product(
                102,
                    "Máquina de Espresso Breville BES870XL/A",
                    "La máquina de Espresso Breville BES870XL/A, debe ser parte de tu vida, porque con ella obtendrás la bebidas más deliciosas para que inicies tu día con la mejor actitud, sin duda esta máquina de Espresso Breville BES870XL/A marcará la diferencia desde el primer instante. Con la máquina de Espresso Breville BES870XL/A contarás con una increíble bomba de diseño innovador, la cual posee 15 bares de presión, además de un sistema de calentamiento Thermocoil, molino cónico que molerá los granos de café en segundos y un texturizador de leche ajustable a 360°. No te pierdas de tener en casa esta máquina de Espresso Breville BES870XL/A, te va a fascinar",
                    R.drawable.cafetera,
                "Cafeteras",
                    15759.20F,
                    5.0F
            ),
            Product(
                103,
                    "iPhone 12 Mini Super Retina XDR, 5.4 Pulgadas",
                    "Este año se repite estrategia y, de nuevo, su modelo intermedio no apuesta por la triple cámara. No obstante, hay un importante (y esperado) salto a nivel de pantalla, y es que el iPhone 12 implementa tecnología OLED y resolución Full HD+ (Super Retina XDR) en un panel de 6.1 pulgadas. La conexión 5G también se hace protagonista en este iPhone 12 que, sobre el papel, presentará una dura batalla por debajo de los mil euros.",
                    R.drawable.iphone,
                "Smartphones",
                    25669F,
                    5F
            ),
            Product(
                104,
                    "Tenis Vans para Hombre Old Skool",
                    "Muestra con el tenis Vans tu fascinante estilo a cada paso. Porque mereces tener el balance perfecto entre estilo y comodidad, solo en Liverpool lo encontrarás. Descubre cada una de sus especiales características como materiales resistentes, punta redonda, fijación de agujeta para un ajuste ideal, así como extraordinaria selección de color contrastante en su diseño, combínalos con las prendas que decidas, desde bermudas, hasta jeans. Atrévete a crear el match ideal",
                    R.drawable.tenis,
                "Tenis",
                    1199F,
                    4.1F
            ),
            Product(
                105,
                    "Elíptica NordicTrack E 7.5 Z",
                    "La elíptica NordicTrack E 7.5 Z será el perfecto complemento para tu gimnasio en casa, ya que cuenta con 20 programas, ideales para elegir el resultado que quieres lograr en tu cuerpo. Utiliza lo mejor de tu energía y músculos gracias a sus 5 niveles de inclinación para mejorar tu entrenamiento; sus manerales suaves al tacto te darán el agarre perfecto además tendrás un perfecta base para poner tu botella de agua y la posibilidad de monitorear tu ritmo cardíaco debido a sus sensores. Elige la elíptica NordicTrack E 7.5 Z y dale a tu rutina un nuevo estilo",
                    R.drawable.eliptica,
                "Deportes",
                    23999F,
                    4.0F
            ),
            Product(
                106,
                    "Escritorio Vigar Piero Contemporáneo de madera",
                    "Escritorio de estructura metálica y cubierta de melanina",
                    R.drawable.escritorio,
                "Artículos de Oficina",
                    6299.40F,
                    4.2F
            ),
            Product(
                107,
                    "Altavoz Inalámbrico Bose S1 Pro negro",
                    "Lleva el altavoz inalámbrico Bose S1 Pro negro ¡a todas partes! Porque este increíble modelo cuenta con una excelente variedad de aplicaciones profesionales que funcionan como altavoz principal, monitor de piso, práctico amplificador y sistema principal de música. Debido a su función de ecualización automática como a su mezcladora de 3 canales este altavoz inalámbrico Bose S1 Pro negro es perfecto para músicos, canta-autores, DJ’s, así como para presentadores. Sin importar de qué forma lo utilices este maravilloso artefacto te brindará un fantástico sonido para tus diferentes actividades artísticas o de entretenimiento",
                    R.drawable.bose,
                "Audio",
                    14400F,
                    4.5F
            )
        )
        products.forEach{
            productDB.add(it)
        }
    }

    override fun onCheckBoxListener(products: ArrayList<Product>) {
        productToModify = products
    }

//    override fun onCheckBoxListener(list: ArrayList<String>) {
////        Toast.makeText(this, "$list", Toast.LENGTH_SHORT).show()
//        productDB.delete()
//    }
}