package com.example.liverpool

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.liverpool.database.ProductDB
import com.example.liverpool.databinding.ActivityToolbarBinding

class ToolbarActivity : AppCompatActivity() {
    private lateinit var binding: ActivityToolbarBinding
    private lateinit var productDB : ProductDB
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityToolbarBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.apply {
            title = "Categorías"
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }

        binding.textViewCategories.text = ""
        productDB = ProductDB(baseContext, "productsBD", TypeConnection.WRITABLE)
        var products = productDB.getAll()

        products.forEach{
            binding.textViewCategories.text = "${binding.textViewCategories.text}${it.category}\n"
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}