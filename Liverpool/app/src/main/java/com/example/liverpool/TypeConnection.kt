package com.example.liverpool

enum class TypeConnection {
    READONLY,
    WRITABLE
}