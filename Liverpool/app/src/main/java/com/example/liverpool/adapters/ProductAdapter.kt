package com.example.liverpool.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.liverpool.R
import com.example.liverpool.adapters.viewholders.ProductViewHolder
import com.example.liverpool.listener.ProductListener
import com.example.liverpool.models.Product

class ProductAdapter(private val products:ArrayList<Product>,
                     private val listener: ProductListener)
    : RecyclerView.Adapter<ProductViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
            val itemView = LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false)
            return ProductViewHolder(itemView, listener)
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bindProduct(products[position])
    }

    override fun getItemCount(): Int {
        return products.size
    }

//    fun add(product: Product) {
//        products.add(product)
//        notifyDataSetChanged()
//    }
//
//    fun edit(index: Int, product: Product) {
//        products.set(index, product)
//        notifyDataSetChanged()
//    }
//
//    fun clear() {
//        products.clear()
//        notifyDataSetChanged()
//    }
}