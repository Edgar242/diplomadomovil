package com.example.liverpool.adapters

import android.util.SparseBooleanArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.liverpool.R
import com.example.liverpool.databinding.ManageProductItemBinding
import com.example.liverpool.listener.ProductMgmtListener
import com.example.liverpool.models.Product

class ProductManagementAdapter(private val products:ArrayList<Product>, private val listener: ProductMgmtListener)
    : RecyclerView.Adapter<ProductManagementAdapter.MyViewHolder>() {

    private lateinit var binding : ManageProductItemBinding
    var checkBoxStateArray = SparseBooleanArray()
    var productsSelected = arrayListOf<Product>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.manage_product_item, parent, false)
        binding = ManageProductItemBinding.bind(itemView)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bindProduct(products[position], position)
    }

    override fun getItemCount(): Int {
        return products.size
    }

    fun add(product: Product) {
        products.add(product)
        notifyDataSetChanged()
    }

    fun edit(index: Int, product: Product) {
        products.set(index, product)
        notifyDataSetChanged()
    }

    fun delete(product: Product) {
        products.remove(product)
        notifyDataSetChanged()
    }

    fun clear() {
        products.clear()
        notifyDataSetChanged()
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindProduct(product: Product, position: Int) {
            binding.textViewItemName.text = product.title
            binding.textViewItemPrice.text = "$"+ product.price.toString()
            binding.ratingBarProduct.rating = product.rating
            binding.imageViewItemImage.setImageResource(product.image)

            binding.checkBox.isChecked = checkBoxStateArray.get(position, false)

            binding.cardViewProduct.setOnClickListener{
                listener.onCheckBoxListener(productsSelected)
            }

            binding.checkBox.setOnClickListener{
                // TODO: Save id in a list and use it for editing or deleting operations
                val state = !checkBoxStateArray.get(adapterPosition, false)
//                binding.checkBox.isChecked = state
                checkBoxStateArray.put(adapterPosition, state)
                if (state) {
                    productsSelected.add(products[adapterPosition])
                } else {
                    productsSelected.remove(products[adapterPosition])
                }

                listener.onCheckBoxListener(productsSelected)
            }

        }
    }
}