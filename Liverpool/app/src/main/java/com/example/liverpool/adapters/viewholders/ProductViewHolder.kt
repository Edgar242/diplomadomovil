package com.example.liverpool.adapters.viewholders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.example.liverpool.databinding.ProductItemBinding
import com.example.liverpool.listener.ProductListener
import com.example.liverpool.models.Product

class ProductViewHolder(itemView: View, listener: ProductListener) : RecyclerView.ViewHolder(itemView) {
    private var productListener = listener
    private val binding = ProductItemBinding.bind(itemView)

    fun bindProduct(product: Product) {
        binding.textViewItemName.text = product.title
        binding.textViewItemPrice.text = "$"+ product.price.toString()
        binding.ratingBarProduct.rating = product.rating
        binding.imageViewItemImage.setImageResource(product.image)

        itemView.setOnClickListener{
            productListener.onClickProduct(product)
        }
    }
}