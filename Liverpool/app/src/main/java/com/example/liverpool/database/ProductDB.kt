package com.example.liverpool.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.liverpool.TypeConnection
import com.example.liverpool.models.Product

class ProductDB(val context: Context,
                val bdName: String,
                val typeConnection: TypeConnection) {

    var mySQLiteDB : SQLiteDatabase? = null

    companion object {
        const val tableProducts = "Products"
    }

    init {
        val dataBaseConnection = ProductDBConnection(context, bdName, null, 1 )
         mySQLiteDB = when (typeConnection) {
             TypeConnection.READONLY -> {
                 dataBaseConnection.readableDatabase
             }
             TypeConnection.WRITABLE -> {
                 dataBaseConnection.writableDatabase
             }
        }
    }

    fun add(product : Product) : Boolean {
        var result : Long =  0
        val productContentValues = ContentValues()
        productContentValues.put("title", product.title)
        productContentValues.put("price", product.price)
        productContentValues.put("description", product.description)
        productContentValues.put("image", product.image)
        productContentValues.put("category", product.category)
        productContentValues.put("price", product.price)
        productContentValues.put("rating", product.rating)

        mySQLiteDB?.let {
            result = it.insert("Products", null,productContentValues)
        }
        return result > 0
    }

    fun update(product: Product): Boolean {
        var result = 0
        val productContentValues = ContentValues()
        productContentValues.put("title", product.title)
        productContentValues.put("price", product.price)
        productContentValues.put("description", product.description)
        productContentValues.put("image", product.image)
        productContentValues.put("category", product.category)
        productContentValues.put("price", product.price)
        productContentValues.put("rating", product.rating)

        val whereClause = "id=?"
        val whereArgs = arrayOf<String>(product.id.toString())
        mySQLiteDB?.let {
            result = it.update(tableProducts,productContentValues,whereClause, whereArgs)
        }
        return result > 0
    }

    fun delete(product: Product): Boolean {
        var result = 0
        val whereClause = "id=?"
        val whereArgs = arrayOf<String>(product.id.toString())
        mySQLiteDB?.let {
            result = it.delete(tableProducts,whereClause, whereArgs)
        }
        return result > 0
    }

    fun getByID(id: Int) : Product? {
        val columns = arrayOf("id", "title", "description", "image", "category", "price", "rating")
        val args = arrayOf(id.toString())
        val clause = "id=?"

        mySQLiteDB?.let {
            val cursor = it.query(tableProducts,
                columns, clause, args, null, null , null, null)
            if (cursor.moveToFirst()) {
                do {
                    return Product(
                        cursor.getInt(0), // id
                        cursor.getString(1), // title
                        cursor.getString(2), // description
                        cursor.getInt(3),    // image
                        cursor.getString(4),   // category
                        cursor.getFloat(5),   // price
                        cursor.getFloat(6),   // rating
                    )
                } while (cursor.moveToNext())
            }
        }
        return null
    }

    fun getAll() : ArrayList<Product> {
        var products = arrayListOf<Product>()
        val columns = arrayOf("id", "title", "description", "image", "category", "price", "rating")

        mySQLiteDB?.let {
            val cursor = it.query(tableProducts,columns,null, null, null, null, null, null)
            if (cursor.moveToFirst()) {
                do {
                    products.add(Product(
                        cursor.getInt(0), // id
                        cursor.getString(1), // title
                        cursor.getString(2), // description
                        cursor.getInt(3),    // image
                        cursor.getString(4),   // category
                        cursor.getFloat(5),   // price
                        cursor.getFloat(6),   // rating
                    ))
                } while (cursor.moveToNext())
            }
        }
        return products
    }

    fun clear() : Boolean {
        var result = 0
        mySQLiteDB?.let {
           result = it.delete(tableProducts, null, null)
        }
        return result > 0
    }
}