package com.example.liverpool.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class ProductDBConnection(context: Context,
                          dbName: String,
                          cursorFactory: SQLiteDatabase.CursorFactory?,
                          version: Int) : SQLiteOpenHelper(context, dbName, cursorFactory, version) {

    val createProductTable = """CREATE TABLE IF NOT EXISTS Products (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            title TEXT NOT NULL DEFAULT "",
            description TEXT NOT NULL DEFAULT "",
            image INTEGER NOT NULL DEFAULT 0,
            category INTEGER NOT NULL DEFAULT 0,
            price REAL NOT NULL DEFAULT 0,
            rating REAL NOT NULL DEFAULT 0
            );"""


    override fun onCreate(db: SQLiteDatabase?) {
        db?.let {
            it.execSQL(createProductTable)
        }
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.let {
            it.execSQL("DROP TABLE IF EXISTS Products")
            it.execSQL(createProductTable)
        }
    }
}