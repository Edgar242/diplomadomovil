package com.example.liverpool.listener

import com.example.liverpool.models.Product

interface ProductListener {

    fun onClickProduct(product: Product)
}