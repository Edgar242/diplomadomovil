package com.example.liverpool.listener

import com.example.liverpool.models.Product

interface ProductMgmtListener {
    fun onCheckBoxListener(products : ArrayList<Product>)
}