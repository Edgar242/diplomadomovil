package com.example.liverpool.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Product(
    val id: Int,
    val title: String,
    val description: String,
    val image: Int,
    val category: String,
    val price: Float,
    val rating: Float,
) : Parcelable
