package com.example.liverpool.models

import android.content.SharedPreferences
import java.math.BigInteger
import java.security.MessageDigest

class User {
    var name: String
    var lastName: String
    var email: String
    var password: String
    var isLogged: Boolean
    private var sharedPref : SharedPreferences


    constructor(sharedPreferences: SharedPreferences) {
        this.sharedPref = sharedPreferences
        this.sharedPref.let {
            this.name = it.getString("name", "")!!
            this.lastName = it.getString("lastName", "")!!
            this.email = it.getString("email", "")!!
            this.password = it.getString("password", "")!!
            this.isLogged = it.getBoolean("isLogged", false)
        }
    }
    companion object {
        const val sharedPrefName = "user"
    }

    fun register(name:String, lastName:String, email:String, password: String) {
        sharedPref.edit().let {
            // TODO: add more user using key
            it.putString("name", name)
            it.putString("lastName", lastName)
            it.putString("email", email)
            it.putString("password", md5Hash(password))
            it.putBoolean("isLogged", true)
            it.commit()
        }
    }

    fun logIn(email: String, password: String) : Boolean {
        val result = (email == sharedPref.getString("email", "")) &&
            (md5Hash(password) == sharedPref.getString("password", ""))
        sharedPref.edit().putBoolean("isLogged", result).apply()
        return result
    }

    fun logOut() {
        sharedPref.edit().putBoolean("isLogged", false).apply()
    }

    fun isEmailRegistered(email: String) : Boolean {
        return email == sharedPref.getString("email", "")
    }

    fun clear() {
        sharedPref.edit().clear().apply()
    }

    private fun md5Hash(str: String): String {
        val md = MessageDigest.getInstance("MD5")
        val bigInt = BigInteger(1, md.digest(str.toByteArray(Charsets.UTF_8)))
        return String.format("%032x", bigInt)
    }
}

