package com.example.ejercicio

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.ejercicio.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Extracting data from bundle
        binding.textViewDataMovieName.text = intent.getStringExtra("name")
        binding.textViewDataHasAdultContent.text = if (intent.getBooleanExtra("adultContent", false)) {
            "Contenido para adultos"
        } else "Para todo público"
        binding.textViewDataGenre.text = "Géneros: " + intent.getStringArrayListExtra("genre")
        binding.textViewDataPublishYear.text = "Año de publicación: " + intent.getStringExtra("year")
        binding.imageViewDataCover.setImageResource(intent.getIntExtra("coverImage", R.drawable.androidparty))

        binding.buttonBack.setOnClickListener() {
            intent = Intent(this, MainActivity::class.java )
            startActivity(intent)
        }
    }
}