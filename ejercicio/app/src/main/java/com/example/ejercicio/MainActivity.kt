package com.example.ejercicio

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import com.example.ejercicio.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    var adultContent: Boolean = false
    //var genre = mutableListOf<String>()
    var genre: ArrayList<String> = arrayListOf()
    var onCheckedChangeListenerRating = CompoundButton.OnCheckedChangeListener{ buttonView, _ -> setGenre(buttonView) }
    var itemYears = arrayOf("2000", "2001", "2002", "2003", "2004", "2005")
    var yearSelected = ""
    var movieName = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)  // Old fashion way
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.radioGroupAdultContent.setOnCheckedChangeListener { _, checkedId ->
            adultContent = checkedId == R.id.radioButtonYes
            //Toast.makeText(this, "Contenido adulto: $adultContent", Toast.LENGTH_SHORT).show()
        }

        binding.checkBoxAction.setOnCheckedChangeListener( onCheckedChangeListenerRating )

        binding.checkBoxComedy.setOnCheckedChangeListener( onCheckedChangeListenerRating )

        binding.checkBoxDrama.setOnCheckedChangeListener( onCheckedChangeListenerRating )

        val yearsAdapter = ArrayAdapter<String>( baseContext, android.R.layout.simple_spinner_item, itemYears )
        yearsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        binding.spinnerYears.adapter = yearsAdapter
        binding.spinnerYears.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                yearSelected = itemYears[position]
                Toast.makeText(baseContext, "Position: $position ", Toast.LENGTH_SHORT).show()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                yearSelected = ""
            }
        }

        binding.textInputLayoutMovieName.editText?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // TODO("Not yet implemented")
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                val maxLength = binding.textInputLayoutMovieName.counterMaxLength
                /*s.let { text ->
                    if (text?.length!! > maxLength) {
                        binding.textInputLayoutMovieName.error = "La cadena llegó a su límite"
                    } else {
                        binding.textInputLayoutMovieName.error = ""
                    }
                }*/
                if (s?.length!! > maxLength) {
                    binding.textInputLayoutMovieName.error = "La cadena llegó a su límite"
                } else {
                    binding.textInputLayoutMovieName.error = ""
                    binding.textInputLayoutMovieName.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable?) {
                /*val name = s?.toString()
                if (name.isNullOrEmpty()) {
                    binding.textInputLayoutMovieName.error = "La cadena es vacia"
                } else {
                    binding.textInputLayoutMovieName.error = ""
                }*/
            }
        })

        binding.buttonSave.setOnClickListener {
            /*var movieName = binding.textInputMovieName.text.toString()
            var nameMovie = binding.textInputLayoutMovieName.editText?.text.toString()
            if (nameMovie.isNotEmpty()) {
                binding.textInputLayoutMovieName.error = ""
            } else {
                binding.textInputLayoutMovieName.error = "No debe de ser vacío"
            }*/

            if (binding.textInputLayoutMovieName.isErrorEnabled) {
                Toast.makeText(baseContext, "Error in name of movie", Toast.LENGTH_SHORT)
            } else {
                val detailIntent = Intent(this, DetailActivity::class.java).apply {
                    val detailBundle = Bundle().apply {
                        putString("name", binding.textInputLayoutMovieName.editText?.text.toString())
                        putBoolean("adultContent", adultContent)
                        putStringArrayList("genre", genre)
                        putString("year", yearSelected)
                        putInt("coverImage", R.drawable.avengers)
                    }
                    putExtras(detailBundle)
                }
                startActivity(detailIntent)
            }
        }
    }

    private fun setGenre(button: CompoundButton) {
        val text = button.text.toString()
        if (genre.contains(text) )
            genre.remove(text)
        else
            genre.add(text)
        //Toast.makeText(this, "$genre size: ${genre.size}", Toast.LENGTH_SHORT).show()
    }
}